import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

const adminMenuItems = (
  <>
    <MenuItem icon="road" to="/admin/gateway">
      Шлюз
    </MenuItem>
    <MenuItem icon="user" to="/admin/user-management">
      Пользователи
    </MenuItem>
    <MenuItem icon="tachometer-alt" to="/admin/metrics">
      Метрики
    </MenuItem>
    <MenuItem icon="heart" to="/admin/health">
      Состояние
    </MenuItem>
    <MenuItem icon="list" to="/admin/configuration">
      Конфигурация
    </MenuItem>
    <MenuItem icon="bell" to="/admin/audits">
      Аудит
    </MenuItem>
    {/* jhipster-needle-add-element-to-admin-menu - JHipster will add entities to the admin menu here */}
    <MenuItem icon="tasks" to="/admin/logs">
      Логи
    </MenuItem>
  </>
);

const swaggerItem = (
  <MenuItem icon="book" to="/admin/docs">
    API
  </MenuItem>
);

const databaseItem = (
  <DropdownItem tag="a" href="./h2-console" target="_tab">
    <FontAwesomeIcon icon="hdd" fixedWidth /> Database
  </DropdownItem>
);

export const AdminMenu = ({ showSwagger, showDatabase }) => (
  <NavDropdown icon="user-plus" name="Администрирование" style={{ width: '140%' }} id="admin-menu">
    {adminMenuItems}
    {showSwagger && swaggerItem}

    {showDatabase && databaseItem}
  </NavDropdown>
);

export default AdminMenu;
