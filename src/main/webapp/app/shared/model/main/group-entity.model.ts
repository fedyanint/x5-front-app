import { IProductEntity } from 'app/shared/model/main/product-entity.model';
import { IClusterEntity } from 'app/shared/model/main/cluster-entity.model';

export interface IGroupEntity {
  id?: number;
  name?: string;
  productEntitySets?: IProductEntity[];
  cluster?: IClusterEntity;
}

export const defaultValue: Readonly<IGroupEntity> = {};
