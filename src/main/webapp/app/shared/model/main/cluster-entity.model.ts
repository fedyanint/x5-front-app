import { IGroupEntity } from 'app/shared/model/main/group-entity.model';

export interface IClusterEntity {
  id?: number;
  name?: string;
  groupEntitySets?: IGroupEntity[];
}

export const defaultValue: Readonly<IClusterEntity> = {};
