import { IGroupEntity } from 'app/shared/model/main/group-entity.model';

export interface IProductEntity {
  id?: number;
  name?: string;
  group?: IGroupEntity;
}

export const defaultValue: Readonly<IProductEntity> = {};
