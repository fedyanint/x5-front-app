import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IClusterEntity } from 'app/shared/model/main/cluster-entity.model';
import { getEntities as getClusterEntities } from 'app/entities/main/cluster-entity/cluster-entity.reducer';
import { getEntity, updateEntity, createEntity, reset } from './group-entity.reducer';
import { IGroupEntity } from 'app/shared/model/main/group-entity.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IGroupEntityUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const GroupEntityUpdate = (props: IGroupEntityUpdateProps) => {
  const [clusterId, setClusterId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { groupEntityEntity, clusterEntities, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/group-entity');
  };

  useEffect(() => {
    if (!isNew) {
      props.getEntity(props.match.params.id);
    }

    props.getClusterEntities();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...groupEntityEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.mainGroupEntity.home.createOrEditLabel">Создать или редактировать Группу товаров</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Загрузка...</p>
          ) : (
            <AvForm model={isNew ? {} : groupEntityEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="group-entity-id">ID</Label>
                  <AvInput id="group-entity-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="group-entity-name">
                  Название
                </Label>
                <AvField id="group-entity-name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label for="group-entity-cluster">Кластер</Label>
                <AvInput id="group-entity-cluster" type="select" className="form-control" name="cluster.id">
                  <option value="" key="0" />
                  {clusterEntities
                    ? clusterEntities.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/group-entity" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Назад</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Сохранить
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  clusterEntities: storeState.clusterEntity.entities,
  groupEntityEntity: storeState.groupEntity.entity,
  loading: storeState.groupEntity.loading,
  updating: storeState.groupEntity.updating,
  updateSuccess: storeState.groupEntity.updateSuccess
});

const mapDispatchToProps = {
  getClusterEntities,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(GroupEntityUpdate);
