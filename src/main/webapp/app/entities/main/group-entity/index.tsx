import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import GroupEntity from './group-entity';
import GroupEntityDetail from './group-entity-detail';
import GroupEntityUpdate from './group-entity-update';
import GroupEntityDeleteDialog from './group-entity-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={GroupEntityDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={GroupEntityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={GroupEntityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={GroupEntityDetail} />
      <ErrorBoundaryRoute path={match.url} component={GroupEntity} />
    </Switch>
  </>
);

export default Routes;
