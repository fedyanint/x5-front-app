import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IGroupEntity, defaultValue } from 'app/shared/model/main/group-entity.model';

export const ACTION_TYPES = {
  FETCH_GROUPENTITY_LIST: 'groupEntity/FETCH_GROUPENTITY_LIST',
  FETCH_GROUPENTITY: 'groupEntity/FETCH_GROUPENTITY',
  CREATE_GROUPENTITY: 'groupEntity/CREATE_GROUPENTITY',
  UPDATE_GROUPENTITY: 'groupEntity/UPDATE_GROUPENTITY',
  DELETE_GROUPENTITY: 'groupEntity/DELETE_GROUPENTITY',
  RESET: 'groupEntity/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IGroupEntity>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type GroupEntityState = Readonly<typeof initialState>;

// Reducer

export default (state: GroupEntityState = initialState, action): GroupEntityState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_GROUPENTITY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_GROUPENTITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_GROUPENTITY):
    case REQUEST(ACTION_TYPES.UPDATE_GROUPENTITY):
    case REQUEST(ACTION_TYPES.DELETE_GROUPENTITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_GROUPENTITY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_GROUPENTITY):
    case FAILURE(ACTION_TYPES.CREATE_GROUPENTITY):
    case FAILURE(ACTION_TYPES.UPDATE_GROUPENTITY):
    case FAILURE(ACTION_TYPES.DELETE_GROUPENTITY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_GROUPENTITY_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_GROUPENTITY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_GROUPENTITY):
    case SUCCESS(ACTION_TYPES.UPDATE_GROUPENTITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_GROUPENTITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/main/api/group-entities';

// Actions

export const getEntities: ICrudGetAllAction<IGroupEntity> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_GROUPENTITY_LIST,
    payload: axios.get<IGroupEntity>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IGroupEntity> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_GROUPENTITY,
    payload: axios.get<IGroupEntity>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IGroupEntity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_GROUPENTITY,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IGroupEntity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_GROUPENTITY,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IGroupEntity> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_GROUPENTITY,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
