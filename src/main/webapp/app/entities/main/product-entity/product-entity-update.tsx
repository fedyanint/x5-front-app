import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IGroupEntity } from 'app/shared/model/main/group-entity.model';
import { getEntities as getGroupEntities } from 'app/entities/main/group-entity/group-entity.reducer';
import { getEntity, updateEntity, createEntity, reset } from './product-entity.reducer';
import { IProductEntity } from 'app/shared/model/main/product-entity.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProductEntityUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductEntityUpdate = (props: IProductEntityUpdateProps) => {
  const [groupId, setGroupId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { productEntityEntity, groupEntities, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/product-entity' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getGroupEntities();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...productEntityEntity,
        ...values
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="frontApp.mainProductEntity.home.createOrEditLabel">Создать или редактировать Товар</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Загрузка...</p>
          ) : (
            <AvForm model={isNew ? {} : productEntityEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="product-entity-id">ID</Label>
                  <AvInput id="product-entity-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="product-entity-name">
                  Название
                </Label>
                <AvField id="product-entity-name" type="text" name="name" />
              </AvGroup>
              <AvGroup>
                <Label for="product-entity-group">Group</Label>
                <AvInput id="product-entity-group" type="select" className="form-control" name="group.id">
                  <option value="" key="0" />
                  {groupEntities
                    ? groupEntities.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/product-entity" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Назад</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Сохранить
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  groupEntities: storeState.groupEntity.entities,
  productEntityEntity: storeState.productEntity.entity,
  loading: storeState.productEntity.loading,
  updating: storeState.productEntity.updating,
  updateSuccess: storeState.productEntity.updateSuccess
});

const mapDispatchToProps = {
  getGroupEntities,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductEntityUpdate);
