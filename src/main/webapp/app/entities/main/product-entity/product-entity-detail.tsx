import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product-entity.reducer';
import { IProductEntity } from 'app/shared/model/main/product-entity.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductEntityDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductEntityDetail = (props: IProductEntityDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { productEntityEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Товар [<b>{productEntityEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Название</span>
          </dt>
          <dd>{productEntityEntity.name}</dd>
          <dt>Group</dt>
          <dd>{productEntityEntity.group ? productEntityEntity.group.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/product-entity" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Назад</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product-entity/${productEntityEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Редактировать</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ productEntity }: IRootState) => ({
  productEntityEntity: productEntity.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductEntityDetail);
