import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProductEntity from './product-entity';
import ProductEntityDetail from './product-entity-detail';
import ProductEntityUpdate from './product-entity-update';
import ProductEntityDeleteDialog from './product-entity-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ProductEntityDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProductEntityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProductEntityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProductEntityDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProductEntity} />
    </Switch>
  </>
);

export default Routes;
