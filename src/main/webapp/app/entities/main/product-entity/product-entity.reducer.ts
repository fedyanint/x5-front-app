import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IProductEntity, defaultValue } from 'app/shared/model/main/product-entity.model';

export const ACTION_TYPES = {
  FETCH_PRODUCTENTITY_LIST: 'productEntity/FETCH_PRODUCTENTITY_LIST',
  FETCH_PRODUCTENTITY: 'productEntity/FETCH_PRODUCTENTITY',
  CREATE_PRODUCTENTITY: 'productEntity/CREATE_PRODUCTENTITY',
  UPDATE_PRODUCTENTITY: 'productEntity/UPDATE_PRODUCTENTITY',
  DELETE_PRODUCTENTITY: 'productEntity/DELETE_PRODUCTENTITY',
  RESET: 'productEntity/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProductEntity>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProductEntityState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductEntityState = initialState, action): ProductEntityState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTENTITY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTENTITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PRODUCTENTITY):
    case REQUEST(ACTION_TYPES.UPDATE_PRODUCTENTITY):
    case REQUEST(ACTION_TYPES.DELETE_PRODUCTENTITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTENTITY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTENTITY):
    case FAILURE(ACTION_TYPES.CREATE_PRODUCTENTITY):
    case FAILURE(ACTION_TYPES.UPDATE_PRODUCTENTITY):
    case FAILURE(ACTION_TYPES.DELETE_PRODUCTENTITY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTENTITY_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTENTITY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PRODUCTENTITY):
    case SUCCESS(ACTION_TYPES.UPDATE_PRODUCTENTITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PRODUCTENTITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/main/api/product-entities';

// Actions

export const getEntities: ICrudGetAllAction<IProductEntity> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTENTITY_LIST,
    payload: axios.get<IProductEntity>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProductEntity> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTENTITY,
    payload: axios.get<IProductEntity>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProductEntity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PRODUCTENTITY,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProductEntity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PRODUCTENTITY,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProductEntity> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PRODUCTENTITY,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
