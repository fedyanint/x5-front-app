import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './cluster-entity.reducer';
import { IClusterEntity } from 'app/shared/model/main/cluster-entity.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClusterEntityDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ClusterEntityDetail = (props: IClusterEntityDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { clusterEntityEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          ClusterEntity [<b>{clusterEntityEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Название</span>
          </dt>
          <dd>{clusterEntityEntity.name}</dd>
        </dl>
        <Button tag={Link} to="/cluster-entity" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Назад</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/cluster-entity/${clusterEntityEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Редактировать</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ clusterEntity }: IRootState) => ({
  clusterEntityEntity: clusterEntity.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ClusterEntityDetail);
