import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClusterEntity from './cluster-entity';
import ClusterEntityDetail from './cluster-entity-detail';
import ClusterEntityUpdate from './cluster-entity-update';
import ClusterEntityDeleteDialog from './cluster-entity-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ClusterEntityDeleteDialog} />
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClusterEntityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClusterEntityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClusterEntityDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClusterEntity} />
    </Switch>
  </>
);

export default Routes;
