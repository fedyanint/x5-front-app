import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IClusterEntity } from 'app/shared/model/main/cluster-entity.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './cluster-entity.reducer';

export interface IClusterEntityDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ClusterEntityDeleteDialog = (props: IClusterEntityDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/cluster-entity');
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.clusterEntityEntity.id);
  };

  const { clusterEntityEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>Подтвердите операцию удаления</ModalHeader>
      <ModalBody id="frontApp.mainClusterEntity.delete.question">Вы уверены что хотите удалить кластер?</ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp; Отменить
        </Button>
        <Button id="jhi-confirm-delete-clusterEntity" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp; Удалить
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ clusterEntity }: IRootState) => ({
  clusterEntityEntity: clusterEntity.entity,
  updateSuccess: clusterEntity.updateSuccess
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ClusterEntityDeleteDialog);
