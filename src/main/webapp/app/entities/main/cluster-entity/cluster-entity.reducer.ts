import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IClusterEntity, defaultValue } from 'app/shared/model/main/cluster-entity.model';

export const ACTION_TYPES = {
  FETCH_CLUSTERENTITY_LIST: 'clusterEntity/FETCH_CLUSTERENTITY_LIST',
  FETCH_CLUSTERENTITY: 'clusterEntity/FETCH_CLUSTERENTITY',
  CREATE_CLUSTERENTITY: 'clusterEntity/CREATE_CLUSTERENTITY',
  UPDATE_CLUSTERENTITY: 'clusterEntity/UPDATE_CLUSTERENTITY',
  DELETE_CLUSTERENTITY: 'clusterEntity/DELETE_CLUSTERENTITY',
  RESET: 'clusterEntity/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClusterEntity>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ClusterEntityState = Readonly<typeof initialState>;

// Reducer

export default (state: ClusterEntityState = initialState, action): ClusterEntityState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLUSTERENTITY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CLUSTERENTITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CLUSTERENTITY):
    case REQUEST(ACTION_TYPES.UPDATE_CLUSTERENTITY):
    case REQUEST(ACTION_TYPES.DELETE_CLUSTERENTITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CLUSTERENTITY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CLUSTERENTITY):
    case FAILURE(ACTION_TYPES.CREATE_CLUSTERENTITY):
    case FAILURE(ACTION_TYPES.UPDATE_CLUSTERENTITY):
    case FAILURE(ACTION_TYPES.DELETE_CLUSTERENTITY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLUSTERENTITY_LIST): {
      const links = parseHeaderForLinks(action.payload.headers.link);

      return {
        ...state,
        loading: false,
        links,
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links),
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    }
    case SUCCESS(ACTION_TYPES.FETCH_CLUSTERENTITY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CLUSTERENTITY):
    case SUCCESS(ACTION_TYPES.UPDATE_CLUSTERENTITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CLUSTERENTITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'services/main/api/cluster-entities';

// Actions

export const getEntities: ICrudGetAllAction<IClusterEntity> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CLUSTERENTITY_LIST,
    payload: axios.get<IClusterEntity>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IClusterEntity> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CLUSTERENTITY,
    payload: axios.get<IClusterEntity>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IClusterEntity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CLUSTERENTITY,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IClusterEntity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CLUSTERENTITY,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IClusterEntity> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CLUSTERENTITY,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
