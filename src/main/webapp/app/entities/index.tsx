import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClusterEntity from './main/cluster-entity';
import GroupEntity from './main/group-entity';
import ProductEntity from './main/product-entity';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}cluster-entity`} component={ClusterEntity} />
      <ErrorBoundaryRoute path={`${match.url}group-entity`} component={GroupEntity} />
      <ErrorBoundaryRoute path={`${match.url}product-entity`} component={ProductEntity} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
