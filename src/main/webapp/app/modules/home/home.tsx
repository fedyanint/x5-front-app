import './home.scss';

import React from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';
import { Row, Col, Alert } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';

export type IHomeProp = StateProps;

export const Home = (props: IHomeProp) => {
  const { account } = props;

  return (
    <Row>
      <Col md="9">
        <h2>Добро пожаловать в реализацию тестового задания по лоту 2!</h2>
        <p className="lead">Это домашняя страница</p>
        {account && account.login ? (
          <div>
            <Alert color="success">Вы вошли как пользователь {account.login}.</Alert>
          </div>
        ) : (
          <div>
            <Alert color="warning">
              Если вы хотите
              <Link to="/login" className="alert-link">
                {' '}
                войти
              </Link>
              , вы можете использовать аккаунты по-умолчанию:
              <br />- Администратор (логин=&quot;admin&quot; и пароль=&quot;admin&quot;)
              <br />- Пользователь (логин=&quot;user&quot; и пароль=&quot;user&quot;).
            </Alert>

            <Alert color="warning">
              У вас еще нет аккаунта?&nbsp;
              <Link to="/account/register" className="alert-link">
                Зарегистрировать аккаунт
              </Link>
            </Alert>
          </div>
        )}
        <p>Если у вас есть вопросы по приложению:</p>

        <ul>
          <li>
            <a href="https://gitlab.com/fedyanint/x5-composite" target="_blank" rel="noopener noreferrer">
              Инструкция данного проекта
            </a>
          </li>
          <li>
            <a href="mailto:tfedyanin@fil-it.ru" target="_blank" rel="noopener noreferrer">
              Почта руководителя разработки - Федянина Тимофея
            </a>
          </li>
        </ul>

        <p>
          Проект реализован на базе фреймворка JHipster{' '}
          <a href="https://github.com/jhipster/generator-jhipster" target="_blank" rel="noopener noreferrer">
            Github
          </a>
          !
        </p>
      </Col>
    </Row>
  );
};

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

type StateProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Home);
