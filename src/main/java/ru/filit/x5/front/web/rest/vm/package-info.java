/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.filit.x5.front.web.rest.vm;
